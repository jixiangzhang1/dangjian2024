package com.company.project.service;
import com.company.project.model.PartyBranch;
import com.company.project.core.Service;

public interface PartyBranchService extends Service<PartyBranch> {

}
