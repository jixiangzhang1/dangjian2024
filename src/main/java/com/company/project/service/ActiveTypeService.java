package com.company.project.service;

import com.company.project.core.Result;
import com.company.project.core.Service;
import com.company.project.model.ActiveType;

public interface ActiveTypeService extends Service<ActiveType> {

    Result detail(Long id);
}
