package com.company.project.service;

import com.company.project.core.Result;
import com.company.project.core.Service;
import com.company.project.model.Active;

import java.util.List;

public interface ActiveService extends Service<Active> {

    Result list(Active active);

    Integer getNumberByUserId(Long userId);

    List<Active> selectByUserId(Long userId);

    Result detail(Long id);

    Result findRandom(Active active);

    List<Active> selectByTypeId(Long id);
}
