package com.company.project.service;

import com.company.project.core.Result;
import com.company.project.core.Service;
import com.company.project.model.ActiveUser;

public interface ActiveUserService extends Service<ActiveUser> {

    Result list(ActiveUser activeUser);

    Result add(ActiveUser activeUser);
}
