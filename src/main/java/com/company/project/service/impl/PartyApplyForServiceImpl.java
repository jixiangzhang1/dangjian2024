package com.company.project.service.impl;

import com.company.project.core.Result;
import com.company.project.core.ResultGenerator;
import com.company.project.dao.PartyApplyForMapper;
import com.company.project.model.PartyApplyFor;
import com.company.project.service.PartyApplyForService;
import com.company.project.core.AbstractService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional
public class PartyApplyForServiceImpl extends AbstractService<PartyApplyFor> implements PartyApplyForService {

    @Resource
    private PartyApplyForMapper partyApplyForMapper;

    @Override
    public Result list(PartyApplyFor partyApplyFor) {

        if (null == partyApplyFor){
            partyApplyFor = new PartyApplyFor();
        }

        PageHelper.startPage(partyApplyFor.getPage() == null ? 0 : partyApplyFor.getPage(), partyApplyFor.getLimit() == null ? 10 : partyApplyFor.getLimit());
        partyApplyFor.setIsDelete(false);
        List<PartyApplyFor> list = partyApplyForMapper.list(partyApplyFor);
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }
}
