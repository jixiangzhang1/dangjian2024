package com.company.project.service.impl;

import com.company.project.core.AbstractService;
import com.company.project.core.Result;
import com.company.project.core.ResultGenerator;
import com.company.project.dao.ActiveTypeMapper;
import com.company.project.model.ActiveType;
import com.company.project.service.ActiveService;
import com.company.project.service.ActiveTypeService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
@Transactional
public class ActiveTypeServiceImpl extends AbstractService<ActiveType> implements ActiveTypeService {

    @Resource
    private ActiveTypeMapper activeTypeMapper;

    @Resource
    private ActiveService activeService;

    @Override
    public Result detail(Long id) {

        if (null == id){
            return ResultGenerator.genSuccessResult(new ActiveType());
        }else {
            ActiveType activeType = findById(id);
            if (null == activeType){
                return ResultGenerator.genSuccessResult(new ActiveType());
            }else {

                activeType.setActiveList(activeService.selectByTypeId(activeType.getId()));
                return ResultGenerator.genSuccessResult(activeType);
            }
        }
    }
}
