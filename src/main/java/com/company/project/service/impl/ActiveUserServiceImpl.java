package com.company.project.service.impl;

import com.company.project.core.AbstractService;
import com.company.project.core.Result;
import com.company.project.core.ResultGenerator;
import com.company.project.dao.ActiveUserMapper;
import com.company.project.model.ActiveUser;
import com.company.project.service.ActiveUserService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional
public class ActiveUserServiceImpl extends AbstractService<ActiveUser> implements ActiveUserService {

    @Resource
    private ActiveUserMapper activeUserMapper;

    @Override
    public Result list(ActiveUser activeUser) {

        if (null == activeUser){
            activeUser = new ActiveUser();
        }

        PageHelper.startPage(activeUser.getPage() == null ? 0 : activeUser.getPage(), activeUser.getLimit() == null ? 10 : activeUser.getLimit());
        activeUser.setIsDelete(false);
        List<ActiveUser> list = activeUserMapper.list(activeUser);
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }

    @Override
    public Result add(ActiveUser activeUser) {

        Integer rows = activeUserMapper.selectCountByUserId(activeUser);
        if (rows > 0){
            return ResultGenerator.genFailResult("不可重复参与活动");
        }

        save(activeUser);
        Result result= ResultGenerator.genSuccessResult();
        result.setData(activeUser);
        return result;
    }
}
