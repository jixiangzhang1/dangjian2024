package com.company.project.service.impl;

import com.company.project.dao.LearningExperienceMapper;
import com.company.project.model.LearningExperience;
import com.company.project.service.LearningExperienceService;
import com.company.project.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
@Transactional
public class LearningExperienceServiceImpl extends AbstractService<LearningExperience> implements LearningExperienceService {

    @Resource
    private LearningExperienceMapper tLearningExperienceMapper;

}
