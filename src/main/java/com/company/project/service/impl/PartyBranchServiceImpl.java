package com.company.project.service.impl;

import com.company.project.dao.PartyBranchMapper;
import com.company.project.model.PartyBranch;
import com.company.project.service.PartyBranchService;
import com.company.project.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
@Transactional
public class PartyBranchServiceImpl extends AbstractService<PartyBranch> implements PartyBranchService {

    @Resource
    private PartyBranchMapper tPartyBranchMapper;

}
