package com.company.project.service.impl;

import com.company.project.core.AbstractService;
import com.company.project.core.Result;
import com.company.project.core.ResultGenerator;
import com.company.project.dao.ActiveMapper;
import com.company.project.model.Active;
import com.company.project.service.ActiveService;
import com.company.project.service.MomentCommentService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;

@Service
@Transactional
public class ActiveServiceImpl extends AbstractService<Active> implements ActiveService {

    @Resource
    private ActiveMapper activeMapper;

    @Autowired
    private MomentCommentService momentCommentService;

    @Override
    public Result list(Active active) {

        if (null == active){
            active = new Active();
        }

        PageHelper.startPage(active.getPage() == null ? 0 : active.getPage(), active.getLimit() == null ? 10 : active.getLimit());
        active.setIsDelete(false);
        List<Active> list = activeMapper.list(active);
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }

    @Override
    public Integer getNumberByUserId(Long userId) {
        return activeMapper.getNumberByUserId(userId);
    }

    @Override
    public List<Active> selectByUserId(Long userId) {
        return activeMapper.selectByUserId(userId);
    }

    @Override
    public Result detail(Long id) {

        if (null == id){
            return ResultGenerator.genSuccessResult(new Active());
        }else {
            Active active = activeMapper.detail(id);
            if (null == active){
                return ResultGenerator.genSuccessResult(new Active());
            }else {

                active.setMomentCommentList(momentCommentService.selectByMomentId(id));
                return ResultGenerator.genSuccessResult(active);
            }
        }
    }

    @Override
    public List<Active> selectByTypeId(Long typeId) {
        return activeMapper.selectByTypeId(typeId);
    }

    @Override
    public Result findRandom(Active active) {
        if (null == active){
            active = new Active();
        }

        PageHelper.startPage(active.getPage() == null ? 0 : active.getPage(), active.getLimit() == null ? 10 : active.getLimit());
        active.setIsDelete(false);
        List<Active> list = activeMapper.list(active);
        Collections.shuffle(list);
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }
}
