package com.company.project.service.impl;

import com.company.project.core.AbstractService;
import com.company.project.core.Result;
import com.company.project.core.ResultGenerator;
import com.company.project.dao.ArticleMapper;
import com.company.project.model.Article;
import com.company.project.service.ArticleService;
import com.company.project.service.MomentCommentService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;

@Service
@Transactional
public class ArticleServiceImpl extends AbstractService<Article> implements ArticleService {

    @Autowired
    private ArticleMapper articleMapper;

    @Autowired
    private MomentCommentService momentCommentService;

    @Override
    public Result add(Article article) {

        save(article);
        Result result= ResultGenerator.genSuccessResult();
        result.setData(article);
        return result;
    }

    @Override
    public List<Article> selectByArticleUserId(Long userId) {
        return articleMapper.selectByArticleUserId(userId);
    }

    @Override
    public Result delete(Long id) {

        Article article=new Article();
        article.setId(id);
        article.setIsDelete(true);
        update(article);

        return ResultGenerator.genSuccessResult();
    }

    @Override
    public List<Article> selectByArticleType(String userId, Long id) {
        List<Article> list = articleMapper.selectByArticleType(userId,id);
        if (null != list && list.size() > 0){
            for (Article d:list) {
                //评论数量
                d.setMomentNumber(momentCommentService.getCountByArticleId(d.getId()));
                d.setMomentCommentList(momentCommentService.selectByMomentId(d.getId()));
            }
        }
        return list;
    }

    @Override
    public Result list(String userId,Article article) {

        if(article == null){
            article = new Article();
        }

        PageHelper.startPage(article.getPage() == null ? 0 : article.getPage(), article.getLimit() == null ? 10 : article.getLimit());
        article.setIsDelete(false);
        List<Article> list = articleMapper.list(article);
        if (list != null && list.size() > 0){
            for (Article d:list) {
                //评论数量
                d.setMomentNumber(momentCommentService.getCountByArticleId(d.getId()));
                d.setMomentCommentList(momentCommentService.selectByMomentId(d.getId()));
            }
        }
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }

    @Override
    public Result detail(Long id, String userId) {
        //根据id查询单个详情
        if (null == id){
            return ResultGenerator.genSuccessResult(new Article());
        }else {
            Article article = articleMapper.detail(id,userId);
            if (null == article){
                return ResultGenerator.genSuccessResult(new Article());
            }else {
                article.setMomentCommentList(momentCommentService.selectByMomentId(article.getId()));

                //数量
                article.setArticleNumber(articleMapper.getCountByUserId(userId));

                //返回查询的单个详情
                return ResultGenerator.genSuccessResult(article);
            }
        }
    }

    @Override
    public List<Article> selectBySiteCateory(Long id) {
        List<Article> list = articleMapper.selectBySiteCateory(id);
        return list;
    }

    @Override
    public Result findByCreatedAt(Article article) {

        if(article == null){
            article = new Article();
        }

        PageHelper.startPage(article.getPage() == null ? 0 : article.getPage(), article.getLimit() == null ? 10 : article.getLimit());
        article.setIsDelete(false);
        List<Article> list = articleMapper.findByCreatedAt(article);
        Collections.shuffle(list);
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }

    @Override
    public Result findByHot(Article article) {

        if(article == null){
            article = new Article();
        }

        PageHelper.startPage(0, 0);
        article.setIsDelete(false);
        List<Article> list = articleMapper.findByHot(article);
        for (Article d:list) {
            d.setMomentCommentList(momentCommentService.selectByMomentId(d.getId()));
        }
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }

    @Override
    public Result findAllByModal(Article article) {

        if(article == null){
            article = new Article();
        }

        PageHelper.startPage(0, 0);
        article.setIsDelete(false);
        List<Article> list = articleMapper.findAllByModal(article);
        for (Article d:list) {
            d.setMomentCommentList(momentCommentService.selectByMomentId(d.getId()));
        }
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }

}
