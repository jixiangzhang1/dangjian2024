package com.company.project.service;
import com.company.project.core.Result;
import com.company.project.model.PartyApplyFor;
import com.company.project.core.Service;

public interface PartyApplyForService extends Service<PartyApplyFor> {

    Result list(PartyApplyFor partyApplyFor);
}
