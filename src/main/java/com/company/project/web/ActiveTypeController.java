package com.company.project.web;

import com.company.project.core.Result;
import com.company.project.core.ResultGenerator;
import com.company.project.model.ActiveType;
import com.company.project.service.ActiveTypeService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/active/type")
@Api(tags = {"/active/type"}, description = "党建活动类型管理模块")
public class ActiveTypeController {
    @Resource
    private ActiveTypeService activeTypeService;

    @ApiOperation(value = "新增", notes = "新增")
    @RequestMapping(value = "/add", method = {RequestMethod.POST})
    public Result add(@RequestBody ActiveType activeType) {
        activeType.setCreatedAt(new Date());
        activeType.setIsDelete(false);
        activeTypeService.save(activeType);
        Result result= ResultGenerator.genSuccessResult();
        result.setData(activeType);
        return result;
    }

    @ApiOperation(value = "删除", notes = "删除")
    @RequestMapping(value = "/delete", method = {RequestMethod.POST})
    public Result delete(@RequestParam Long id) {
        ActiveType activeType=new ActiveType();
        activeType.setId(id);
        activeType.setIsDelete(true);
        activeTypeService.update(activeType);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation(value = "修改", notes = "修改")
    @RequestMapping(value = "/update", method = {RequestMethod.POST})
    public Result update(@RequestBody ActiveType activeType) {
        activeType.setUpdatedAt(new Date());
        activeTypeService.update(activeType);
        Result result= ResultGenerator.genSuccessResult();
        result.setData(activeType);
        return result;
    }

    @ApiOperation(value = "获取单个详情", notes = "获取单个详情")
    @RequestMapping(value = "/detail", method = {RequestMethod.POST})
    public Result detail(@RequestParam(value = "id", required = false) Long id) {
        return activeTypeService.detail(id);
    }

    @ApiOperation(value = "分页查询", notes = "分页查询")
    @RequestMapping(value = "/findByModal", method = {RequestMethod.POST})
    public Result list(@RequestBody(required =false) ActiveType activeType) {

        if (null == activeType){
            activeType = new ActiveType();
        }

        PageHelper.startPage(activeType.getPage() == null ? 0 : activeType.getPage(), activeType.getLimit() == null ? 10 : activeType.getLimit());
        activeType.setIsDelete(false);
        List<ActiveType> list = activeTypeService.findByModel(activeType);
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }
}
