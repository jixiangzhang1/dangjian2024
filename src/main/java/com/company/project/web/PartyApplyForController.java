package com.company.project.web;
import com.company.project.core.Result;
import com.company.project.core.ResultGenerator;
import com.company.project.model.PartyApplyFor;
import com.company.project.service.PartyApplyForService;
import com.company.project.common.BaseController;
import io.swagger.annotations.Api;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;

import javax.annotation.Resource;
import java.util.List;
import java.util.Date;

@RestController
@RequestMapping("/party/apply/for")
@Api(tags = {"/party/apply/for"}, description = "入党申请管理模块")
public class PartyApplyForController extends BaseController{
    @Resource
    private PartyApplyForService partyApplyForService;

    @ApiOperation(value = "新增", notes = "新增")
    @RequestMapping(value = "/add", method = {RequestMethod.POST})
    public Result add(@RequestBody PartyApplyFor partyApplyFor) {
        partyApplyFor.setCreatedAt(new Date());
        partyApplyFor.setIsDelete(false);
        partyApplyFor.setCreatedBy(super.getUserId());
        partyApplyForService.save(partyApplyFor);
        Result result=ResultGenerator.genSuccessResult();
        result.setData(partyApplyFor);
        return result;
    }

    @ApiOperation(value = "删除", notes = "删除")
    @RequestMapping(value = "/delete", method = {RequestMethod.POST})
    public Result delete(@RequestParam Long id) {
        PartyApplyFor partyApplyFor=new PartyApplyFor();
        partyApplyFor.setId(id);
        partyApplyFor.setIsDelete(true);
        partyApplyForService.update(partyApplyFor);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation(value = "修改", notes = "修改")
    @RequestMapping(value = "/update", method = {RequestMethod.POST})
    public Result update(@RequestBody PartyApplyFor partyApplyFor) {
        partyApplyFor.setUpdatedAt(new Date());
        partyApplyForService.update(partyApplyFor);
        Result result=ResultGenerator.genSuccessResult();
        result.setData(partyApplyFor);
        return result;
    }

    @ApiOperation(value = "获取单个详情", notes = "获取单个详情")
    @RequestMapping(value = "/detail", method = {RequestMethod.POST})
    public Result detail(@RequestParam(value = "id", required = false) Long id) {
        if (null == id){
            return ResultGenerator.genSuccessResult(new PartyApplyFor());
        }else {
            PartyApplyFor partyApplyFor = partyApplyForService.findById(id);
            if (null == partyApplyFor){
                return ResultGenerator.genSuccessResult(new PartyApplyFor());
            }else {
                return ResultGenerator.genSuccessResult(partyApplyFor);
            }
        }
    }

    @ApiOperation(value = "分页查询", notes = "分页查询")
    @RequestMapping(value = "/findByModal", method = {RequestMethod.POST})
    public Result list(@RequestBody(required =false) PartyApplyFor partyApplyFor) {
        return partyApplyForService.list(partyApplyFor);
    }
}
