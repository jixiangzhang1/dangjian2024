package com.company.project.web;

import com.company.project.common.BaseController;
import com.company.project.core.Result;
import com.company.project.core.ResultGenerator;
import com.company.project.model.Active;
import com.company.project.service.ActiveService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;

@RestController
@RequestMapping("/active")
@Api(tags = {"/active"}, description = "党建活动管理模块")
public class ActiveController extends BaseController {
    @Resource
    private ActiveService activeService;

    @ApiOperation(value = "新增", notes = "新增")
    @RequestMapping(value = "/add", method = {RequestMethod.POST})
    public Result add(@RequestBody Active active) {
        active.setCreatedAt(new Date());
        active.setIsDelete(false);
        active.setCreatedBy(super.getUserId());
        activeService.save(active);
        Result result= ResultGenerator.genSuccessResult();
        result.setData(active);
        return result;
    }

    @ApiOperation(value = "删除", notes = "删除")
    @RequestMapping(value = "/delete", method = {RequestMethod.POST})
    public Result delete(@RequestParam Long id) {
        Active active=new Active();
        active.setId(id);
        active.setIsDelete(true);
        activeService.update(active);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation(value = "修改", notes = "修改")
    @RequestMapping(value = "/update", method = {RequestMethod.POST})
    public Result update(@RequestBody Active active) {
        activeService.update(active);
        Result result= ResultGenerator.genSuccessResult();
        result.setData(active);
        return result;
    }

    @ApiOperation(value = "获取单个详情", notes = "获取单个详情")
    @RequestMapping(value = "/detail", method = {RequestMethod.POST})
    public Result detail(@RequestParam(value = "id", required = false) Long id) {
        return activeService.detail(id);
    }

    @ApiOperation(value = "分页查询", notes = "分页查询")
    @RequestMapping(value = "/findByModal", method = {RequestMethod.POST})
    public Result list(@RequestBody(required =false) Active active) {
        return activeService.list(active);
    }

    @ApiOperation(value = "分页查询随机", notes = "分页查询随机")
    @RequestMapping(value = "/findRandom", method = {RequestMethod.POST})
    public Result findRandom(@RequestBody(required =false) Active active) {
        return activeService.findRandom(active);
    }
}
