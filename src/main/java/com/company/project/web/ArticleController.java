package com.company.project.web;

import com.company.project.common.BaseController;
import com.company.project.core.Result;
import com.company.project.core.ResultGenerator;
import com.company.project.model.Article;
import com.company.project.service.ArticleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping("/article")
@Api(tags = {"/article"}, description = "博文管理模块")
public class ArticleController extends BaseController {
    @Autowired
    private ArticleService articleService;

    @ApiOperation(value = "新增", notes = "新增")
    @RequestMapping(value = "/add", method = {RequestMethod.POST})
    public Result add(@RequestBody Article article) {
        article.setCreatedAt(new Date());
        article.setIsDelete(false);
        article.setCreatedBy(super.getUserId());
        return articleService.add(article);
    }

    @ApiOperation(value = "删除", notes = "删除")
    @RequestMapping(value = "/delete", method = {RequestMethod.POST})
    public Result delete(@RequestParam Long id) {
        return articleService.delete(id);
    }

    @ApiOperation(value = "修改", notes = "修改")
    @RequestMapping(value = "/update", method = {RequestMethod.POST})
    public Result update(@RequestBody Article article) {
        article.setUpdatedAt(new Date());
        articleService.update(article);
        Result result= ResultGenerator.genSuccessResult();
        result.setData(article);
        return result;
    }

    @ApiOperation(value = "获取单个详情", notes = "获取单个详情")
    @RequestMapping(value = "/detail", method = {RequestMethod.POST})
    public Result detail(@RequestParam(value = "id", required = false) Long id) {
        return articleService.detail(id,super.getUserId());
    }

    @ApiOperation(value = "分页查询", notes = "分页查询")
    @RequestMapping(value = "/findByModal", method = {RequestMethod.POST})
    public Result list(@RequestBody(required =false) Article article) {
        return articleService.list(super.getUserId(),article);
    }

    @ApiOperation(value = "分页查询最新", notes = "分页查询最新")
    @RequestMapping(value = "/findByCreatedAt", method = {RequestMethod.POST})
    public Result findByCreatedAt(@RequestBody(required =false) Article article) {
        return articleService.findByCreatedAt(article);
    }

    @ApiOperation(value = "分页查询热门", notes = "分页查询热门")
    @RequestMapping(value = "/findByHot", method = {RequestMethod.POST})
    public Result findByHot(@RequestBody(required =false) Article article) {
        return articleService.findByHot(article);
    }

    @ApiOperation(value = "分页查询推荐", notes = "分页查询推荐")
    @RequestMapping(value = "/recommendByUser", method = {RequestMethod.POST})
    public Result recommendByUser(@RequestBody(required = false) Article article) {
        return articleService.findByHot(article);
    }

    @ApiOperation(value = "分页查询全部", notes = "分页查询全部")
    @RequestMapping(value = "/findAllByModal", method = {RequestMethod.POST})
    public Result findAllByModal(@RequestBody(required = false) Article article) {
        return articleService.findAllByModal(article);
    }

}
