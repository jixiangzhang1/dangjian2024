package com.company.project.web;
import com.company.project.core.Result;
import com.company.project.core.ResultGenerator;
import com.company.project.model.PartyBranch;
import com.company.project.service.PartyBranchService;
import com.company.project.common.BaseController;
import io.swagger.annotations.Api;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;

import javax.annotation.Resource;
import java.util.List;
import java.util.Date;

@RestController
@RequestMapping("/party/branch")
@Api(tags = {"/party/branch"}, description = "党支部管理模块")
public class PartyBranchController extends BaseController{
    @Resource
    private PartyBranchService partyBranchService;

    @ApiOperation(value = "新增", notes = "新增")
    @RequestMapping(value = "/add", method = {RequestMethod.POST})
    public Result add(@RequestBody PartyBranch partyBranch) {
        partyBranch.setCreatedAt(new Date());
        partyBranch.setIsDelete(false);
        partyBranch.setCreatedBy(super.getUserId());
        partyBranchService.save(partyBranch);
        Result result=ResultGenerator.genSuccessResult();
        result.setData(partyBranch);
        return result;
    }

    @ApiOperation(value = "删除", notes = "删除")
    @RequestMapping(value = "/delete", method = {RequestMethod.POST})
    public Result delete(@RequestParam Long id) {
        PartyBranch partyBranch=new PartyBranch();
        partyBranch.setId(id);
        partyBranch.setIsDelete(true);
        partyBranchService.update(partyBranch);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation(value = "修改", notes = "修改")
    @RequestMapping(value = "/update", method = {RequestMethod.POST})
    public Result update(@RequestBody PartyBranch partyBranch) {
        partyBranch.setUpdatedAt(new Date());
        partyBranchService.update(partyBranch);
        Result result=ResultGenerator.genSuccessResult();
        result.setData(partyBranch);
        return result;
    }

    @ApiOperation(value = "获取单个详情", notes = "获取单个详情")
    @RequestMapping(value = "/detail", method = {RequestMethod.POST})
    public Result detail(@RequestParam(value = "id", required = false) Long id) {
        if (null == id){
            return ResultGenerator.genSuccessResult(new PartyBranch());
        }else {
            PartyBranch partyBranch = partyBranchService.findById(id);
            if (null == partyBranch){
                return ResultGenerator.genSuccessResult(new PartyBranch());
            }else {
                return ResultGenerator.genSuccessResult(partyBranch);
            }
        }
    }

    @ApiOperation(value = "分页查询", notes = "分页查询")
    @RequestMapping(value = "/findByModal", method = {RequestMethod.POST})
    public Result list(@RequestBody(required =false) PartyBranch partyBranch) {

        if (null == partyBranch){
            partyBranch = new PartyBranch();
        }

        PageHelper.startPage(partyBranch.getPage() == null ? 0 : partyBranch.getPage(), partyBranch.getLimit() == null ? 10 : partyBranch.getLimit());
        partyBranch.setIsDelete(false);
        List<PartyBranch> list = partyBranchService.findByModel(partyBranch);
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }
}
