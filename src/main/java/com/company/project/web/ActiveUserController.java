package com.company.project.web;

import com.company.project.core.Result;
import com.company.project.core.ResultGenerator;
import com.company.project.model.ActiveUser;
import com.company.project.service.ActiveUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;

@RestController
@RequestMapping("/active/user")
@Api(tags = {"/active/user"}, description = "党建活动参与管理模块")
public class ActiveUserController {
    @Resource
    private ActiveUserService activeUserService;

    @ApiOperation(value = "新增", notes = "新增")
    @RequestMapping(value = "/add", method = {RequestMethod.POST})
    public Result add(@RequestBody ActiveUser activeUser) {
        activeUser.setCreatedAt(new Date());
        activeUser.setIsDelete(false);
        return activeUserService.add(activeUser);
    }

    @ApiOperation(value = "删除", notes = "删除")
    @RequestMapping(value = "/delete", method = {RequestMethod.POST})
    public Result delete(@RequestParam Long id) {
        ActiveUser activeUser=new ActiveUser();
        activeUser.setId(id);
        activeUser.setIsDelete(true);
        activeUserService.update(activeUser);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation(value = "修改", notes = "修改")
    @RequestMapping(value = "/update", method = {RequestMethod.POST})
    public Result update(@RequestBody ActiveUser activeUser) {
        activeUser.setUpdatedAt(new Date());
        activeUserService.update(activeUser);
        Result result= ResultGenerator.genSuccessResult();
        result.setData(activeUser);
        return result;
    }

    @ApiOperation(value = "获取单个详情", notes = "获取单个详情")
    @RequestMapping(value = "/detail", method = {RequestMethod.POST})
    public Result detail(@RequestParam(value = "id", required = false) Long id) {
        if (null == id){
            return ResultGenerator.genSuccessResult(new ActiveUser());
        }else {
            ActiveUser activeUser = activeUserService.findById(id);
            if (null == activeUser){
                return ResultGenerator.genSuccessResult(new ActiveUser());
            }else {
                return ResultGenerator.genSuccessResult(activeUser);
            }
        }
    }

    @ApiOperation(value = "分页查询", notes = "分页查询")
    @RequestMapping(value = "/findByModal", method = {RequestMethod.POST})
    public Result list(@RequestBody(required =false) ActiveUser activeUser) {
        return activeUserService.list(activeUser);
    }
}
