package com.company.project.web;

import com.company.project.common.BaseController;
import com.company.project.core.Result;
import com.company.project.core.ResultGenerator;
import com.company.project.model.ArticleType;
import com.company.project.service.ArticleTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping("/article/type")
@Api(tags = {"/article/type"}, description = "博文分类管理模块")
public class ArticleTypeController extends BaseController {
    @Autowired
    private ArticleTypeService articleTypeService;

    @ApiOperation(value = "新增", notes = "新增")
    @RequestMapping(value = "/add", method = {RequestMethod.POST})
    public Result add(@RequestBody ArticleType articleType) {
        articleType.setCreatedAt(new Date());
        articleType.setIsDelete(false);
        articleTypeService.save(articleType);
        Result result= ResultGenerator.genSuccessResult();
        result.setData(articleType);
        return result;
    }

    @ApiOperation(value = "删除", notes = "删除")
    @RequestMapping(value = "/delete", method = {RequestMethod.POST})
    public Result delete(@RequestParam Long id) {
        ArticleType articleType=new ArticleType();
        articleType.setId(id);
        articleType.setIsDelete(true);
        articleTypeService.update(articleType);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation(value = "修改", notes = "修改")
    @RequestMapping(value = "/update", method = {RequestMethod.POST})
    public Result update(@RequestBody ArticleType articleType) {
        articleType.setUpdatedAt(new Date());
        articleTypeService.update(articleType);
        Result result= ResultGenerator.genSuccessResult();
        result.setData(articleType);
        return result;
    }

    @ApiOperation(value = "获取单个详情", notes = "获取单个详情")
    @RequestMapping(value = "/detail", method = {RequestMethod.POST})
    public Result detail(@RequestParam(value = "id", required = false) Long id) {
        return articleTypeService.detail(super.getUserId(),id);
    }

    @ApiOperation(value = "分页查询", notes = "分页查询")
    @RequestMapping(value = "/findByModal", method = {RequestMethod.POST})
    public Result list(@RequestBody(required =false) ArticleType articleType) {
        return articleTypeService.list(articleType);
    }

    @ApiOperation(value = "分页查询全部", notes = "分页查询全部")
    @RequestMapping(value = "/findAllByModal", method = {RequestMethod.POST})
    public Result findAllByModal(@RequestBody(required =false) ArticleType articleType) {
        return articleTypeService.findAllByModal(articleType);
    }

    @ApiOperation(value = "查询全部二级分类下", notes = "查询全部二级分类下")
    @RequestMapping(value = "/findTwoByModal", method = {RequestMethod.POST})
    public Result findTwoByModal(@RequestBody(required =false) ArticleType articleType) {
        articleType.setCreatedBy(super.getUserId());
        return articleTypeService.findTwoByModal(articleType);
    }
}
