package com.company.project.web;
import com.company.project.core.Result;
import com.company.project.core.ResultGenerator;
import com.company.project.model.LearningExperience;
import com.company.project.service.LearningExperienceService;
import com.company.project.common.BaseController;
import io.swagger.annotations.Api;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;

import javax.annotation.Resource;
import java.util.List;
import java.util.Date;

@RestController
@RequestMapping("/learning/experience")
@Api(tags = {"/learning/experience"}, description = "学习心得管理模块")
public class LearningExperienceController extends BaseController{
    @Resource
    private LearningExperienceService learningExperienceService;

    @ApiOperation(value = "新增", notes = "新增")
    @RequestMapping(value = "/add", method = {RequestMethod.POST})
    public Result add(@RequestBody LearningExperience learningExperience) {
        learningExperience.setCreatedAt(new Date());
        learningExperience.setIsDelete(false);
        learningExperience.setCreatedBy(super.getUserId());
        learningExperienceService.save(learningExperience);
        Result result=ResultGenerator.genSuccessResult();
        result.setData(learningExperience);
        return result;
    }

    @ApiOperation(value = "删除", notes = "删除")
    @RequestMapping(value = "/delete", method = {RequestMethod.POST})
    public Result delete(@RequestParam Long id) {
        LearningExperience learningExperience=new LearningExperience();
        learningExperience.setId(id);
        learningExperience.setIsDelete(true);
        learningExperienceService.update(learningExperience);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation(value = "修改", notes = "修改")
    @RequestMapping(value = "/update", method = {RequestMethod.POST})
    public Result update(@RequestBody LearningExperience learningExperience) {
        learningExperience.setUpdatedAt(new Date());
        learningExperienceService.update(learningExperience);
        Result result=ResultGenerator.genSuccessResult();
        result.setData(learningExperience);
        return result;
    }

    @ApiOperation(value = "获取单个详情", notes = "获取单个详情")
    @RequestMapping(value = "/detail", method = {RequestMethod.POST})
    public Result detail(@RequestParam(value = "id", required = false) Long id) {
        if (null == id){
            return ResultGenerator.genSuccessResult(new LearningExperience());
        }else {
            LearningExperience learningExperience = learningExperienceService.findById(id);
            if (null == learningExperience){
                return ResultGenerator.genSuccessResult(new LearningExperience());
            }else {
                return ResultGenerator.genSuccessResult(learningExperience);
            }
        }
    }

    @ApiOperation(value = "分页查询", notes = "分页查询")
    @RequestMapping(value = "/findByModal", method = {RequestMethod.POST})
    public Result list(@RequestBody(required =false) LearningExperience learningExperience) {

        if (null == learningExperience){
            learningExperience = new LearningExperience();
        }

        PageHelper.startPage(learningExperience.getPage() == null ? 0 : learningExperience.getPage(), learningExperience.getLimit() == null ? 10 : learningExperience.getLimit());
        learningExperience.setIsDelete(false);
        List<LearningExperience> list = learningExperienceService.findByModel(learningExperience);
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }
}
