package com.company.project.dao;

import com.company.project.core.Mapper;
import com.company.project.model.ActiveUser;

import java.util.List;

public interface ActiveUserMapper extends Mapper<ActiveUser> {

    List<ActiveUser> list(ActiveUser activeUser);

    Integer selectCountByUserId(ActiveUser activeUser);
}