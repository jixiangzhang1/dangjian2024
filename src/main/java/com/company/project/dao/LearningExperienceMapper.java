package com.company.project.dao;

import com.company.project.core.Mapper;
import com.company.project.model.LearningExperience;

public interface LearningExperienceMapper extends Mapper<LearningExperience> {
}