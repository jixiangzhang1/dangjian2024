package com.company.project.dao;

import com.company.project.core.Mapper;
import com.company.project.model.PartyBranch;

public interface PartyBranchMapper extends Mapper<PartyBranch> {
}