package com.company.project.dao;

import com.company.project.core.Mapper;
import com.company.project.model.Active;

import java.util.List;

public interface ActiveMapper extends Mapper<Active> {

    List<Active> list(Active active);

    Integer getNumberByUserId(Long userId);

    List<Active> selectByUserId(Long userId);

    Active detail(Long id);

    List<Active> selectByTypeId(Long typeId);
}