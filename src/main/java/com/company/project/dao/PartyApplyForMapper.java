package com.company.project.dao;

import com.company.project.core.Mapper;
import com.company.project.model.PartyApplyFor;

import java.util.List;

public interface PartyApplyForMapper extends Mapper<PartyApplyFor> {

    List<PartyApplyFor> list(PartyApplyFor partyApplyFor);

}